# Playerfactions_ext

Playerfactions_ext is an extension of the [Playerfactions](https://github.com/mt-mods/playerfactions) mod which adds sub-commands that allow you to teleport, give/withdraw privs and/or items to all members of a faction at the same time or for one player in a faction.

They were used as part of the [UNEJ project](https://minetest.net/education/), which uses Minetest with students to rework on real areas in France. This was made possible thanks to the [IGN (French  National Institute of Geographic and Forest Information) service](https://minecraft.ign.fr/), which allows to create Minetest maps from from geographical data of France or the world (OSM data).

**Requirements**: Playerfactions mod must be installed.

## Commands

### `/factions teleport <player_name> <player_name|factions>`

This is an increased `/teleport <player_name> <player_name>` function, accepting to teleport a player or a faction to another player.


### `/factions grant <player_name|factions> <privs|privs_group|all>`

This is an increased `/grant <player> <privs|all>` function, accepting to give a privs, a group of privs or all privs to a player, a faction or to every connected players.
  - privs_group are initialized in **/worldpath/privs_group.conf**
  - factions will be integrate soon

### `/factions revoke <player_name|everyone|factions> <privs|privs_group|all>`

  This is an increased `/revoke <player> <privs|all>` function, accepting to revoke a privs, a group of privs or all privs to a player, a faction or to every connected players.
  - privs_group is a list of string and is initialized in **/worldpath/privs_group.conf**
  - factions will be integrate soon

### `/factions empty_inventory <player|faction>`

  Empties inventory of a player or a faction.

### `/factions give <player|faction> <identifier [\<amount\>[ \<wear\>]]|package>`

  Gives an item or a package to a player or a faction.
  This is an extension of the basic `/give` command.
    <amount>: 1-65535, defaults to 1 is optional, only affects stackable nodes, has no effect otherwise (e.g. tools)
    <wear>: 0-65535, defaults to 0 (intact), requires amount to be specified, only affects tools.
    Configuration file **<world directory>/unej/manage_give.conf** example:

```
      { "packages": 
        { 
          "my_package": [
            "default:brick 100",
            "default:pick_steel 1 30000"
          ] 
        }
      }
```

### `/factions equip <player|faction> <identifier [\<amount\>[ \<wear\>]]|package>`

  Replaces inventory for a player or a faction.
  Have the same parameters than `u_give`.
  Replaces player inventory instead of adding to it

### `/factions packages [\<package\>]`

  List and displays information on packages.
  List available packages (if no argument is given) or the contents of designated package.


## License

Licensed under the AGPL (Affero General Public License).

## Funding

This mod is published with [funding from the NLNet Foundation](https://nlnet.nl/project/MinetestEdu/).

