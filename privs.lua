local S = minetest.get_translator("playerfactions_ext")

local registered_priv_groups = {}

factions_ext.register_privilege("playerfactions_grant", {description = S("unrestricted use of factions grant sub commands") , give_to_singleplayer = false})

-- Syntax of priv_groups in conf file is list-like for user-friendliness: ["foo", "bar"] -> { 1: "foo", 2: "bar" }
-- This converts it to table-like for convenience : { "foo": true, "bar": true }
-- It also performs validity checks and registers a specific privilege for each priv_group
local conf_priv_groups = (factions_ext.open_file(factions_ext.privs_config_filepath, false) or { priv_groups= {} })["priv_groups"]
if type(conf_priv_groups) == "table" then
	for priv_group_name, priv_group_list in pairs(conf_priv_groups) do
		if type(priv_group_list) == "table" then
			if not registered_priv_groups[priv_group_name] then
				registered_priv_groups[priv_group_name] = {}
				for index, priv_name in pairs(priv_group_list) do
					if type(priv_name) == "string" then
						registered_priv_groups[priv_group_name][priv_name] = true
					else
						registered_priv_groups[priv_group_name] = nil
						minetest.log("warning","factions -!- invalid privilege: "
						.. priv_group_name .. " [" .. tostring(index) .. "]. Priv_group dismissed")
						break
					end
				end
				if registered_priv_groups[priv_group_name] then
					factions_ext.register_privilege(
						"playerfactions_grant_" .. priv_group_name,
						{ description = S("may grant @1", priv_group_name),
						give_to_singleplayer = false })
				end
			else
				minetest.log("warning", "factions -!- priv_group already registered: ".. priv_group_name .. ".")
			end
		else
			minetest.log("warning","factions -!- invalid priv_group: " .. priv_group_name .. ".")
		end
	end
else
	minetest.log("warning","factions -!- could not load registered priv_groups from " .. factions_ext.privs_config_filepath .. ".")
end

local function authorized_priv_group_names(name)
	local priv_group_names = {}
	for priv_group_name in pairs(registered_priv_groups) do
		if minetest.check_player_privs(name, "playerfactions_admin")
		  or minetest.check_player_privs(name, "playerfactions_grant")
		  or minetest.check_player_privs(name, "playerfactions_grant_" .. priv_group_name) then
			priv_group_names[priv_group_name] = true
		end
	end
	return priv_group_names
end

local function authorize_priv(name, priv_name)
	if minetest.registered_privileges[priv_name] then
		if minetest.check_player_privs(name, "playerfactions_admin")
		  or minetest.check_player_privs(name, "playerfactions_grant")
		  or minetest.check_player_privs(name, "playerfactions_grant_" .. priv_name) then
			return true
		end
		for authorized_priv_group in pairs(authorized_priv_group_names(name)) do
			for priv in pairs(registered_priv_groups[authorized_priv_group]) do
				if priv == priv_name then return true end
			end
		end
	end
	return false
end

local function authorize_priv_group(name, priv_group_name)
	return (minetest.check_player_privs(name, "playerfactions_admin")
	    or minetest.check_player_privs(name, "playerfactions_grant")
		or minetest.check_player_privs(name, "playerfactions_grant_" .. priv_group_name))
		and registered_priv_groups[priv_group_name]
end

local function provision_privs(name, query)
	if authorize_priv(name, query) then return { [query] = true } end
	if authorize_priv_group(name, query) then return registered_priv_groups[query] end
	return nil, S("Privilege or priv_group @1 not found or not allowed", query)
end

local function toggle_privs(player_names, toggled_privs, new_value)
	local names_by_outcome = {}
	if not next(toggled_privs) then return false, S("No privilege to grant.") end
	if not next(player_names) then return false, S("No player to affect.") end
	for player_name in pairs(player_names) do
		local player_privs, some_success, some_failure = minetest.get_player_privs(player_name), false, false
		for priv_name in pairs(toggled_privs) do
			if player_privs[priv_name] == new_value then
				some_failure = true
			else
				player_privs[priv_name] = new_value
				some_success = true
			end
		end
		minetest.set_player_privs(player_name, player_privs)
		factions_ext.register_outcome(
			names_by_outcome,
			player_name,
			(new_value and (some_success and (some_failure and S("Some privilege(s) granted") or S("All privilege(s) granted")) or S("None of the privilege(s) granted")))
			  or (some_success and (some_failure and S("Some privilege(s) revoked") or S("All privilege(s) revoked")) or S("None of the privilege(s) revoked"))
		)
	end
	return true, factions_ext.summarize_iteration(names_by_outcome)
end

local grant_description=S("Extends the built-in /grant command. Recipients can be a player or a faction.\
One can grant a single privilege or a group of privileges specified in <world directory>/factions/playerfactions.conf\
example syntax for conf file: { \"priv_groups\": { \"basic\": { \"fly\": \"true\", \"interact\": \"true\" } } }")

factions_ext.register_subcommand("grant", {
	params = "<player_name|faction [and_me]> <privilege|privs_group>",
	description = grant_description,
	func = function(name, action, parsed_params, params)
		local player_names, query = factions_ext.process_names(name, params, "grant")
		if not player_names then return false, query end
		local privs, reason = provision_privs(name, query)
		if not privs then return false, reason end
		return toggle_privs(player_names, privs, true)
	end,
})

factions_ext.register_subcommand("grantme", {
	params = "<privilege|privs_group>",
	description = S("Same as grant, but targets you"),
	func = function(name, action, parsed_params, query)
		local privs, reason = provision_privs(name, query)
		if not privs then return false, reason end
		return toggle_privs({ [name] = true }, privs, true)
	end,
})

factions_ext.register_subcommand("revoke", {
	params = "<player_name|factions [and_me]> <privs|privs_group>",
	description = S("Revoke a given privilege or a group of privileges from a player or a faction"),
	func = function(name, action, parsed_params, params)
		local player_names, query = factions_ext.process_names(name, params, "grant")
		if not player_names then return false, query end
		local privs, reason = provision_privs(name, query)
		if not privs then return false, reason end
		return toggle_privs(player_names, privs, nil)
	end,
})

factions_ext.register_subcommand("priv_groups", {
	params = "[<priv_group>]",
	description = S("List available priv_groups (if no argument is given) or the contents of designated priv_group"),
	func = function(name, action, parsed_params, params)
		local priv_group_name = string.match(params, "([%S]+)")
		local summary
		if priv_group_name then -- list priv_group contents
			if authorize_priv_group(name, priv_group_name) then
				summary = S("Priv_group @1 consists of:", priv_group_name)
				for priv in pairs(registered_priv_groups[priv_group_name]) do
					summary = summary .. " " .. priv
				end
			else
				return false, S("Priv_group @1 is not available", priv_group_name)
			end
		else --list packages
			summary = "Available priv_groups:"
			for authorized_priv_group in pairs(authorized_priv_group_names(name)) do
				summary = summary .. " " .. authorized_priv_group
			end
		end
		return true, summary
	end
})
