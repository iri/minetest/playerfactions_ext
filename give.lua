local registered_packages, conf_packages = {}, {}

local S = minetest.get_translator("playerfactions_ext")

local function filter_registered_stacks(stack_list)
    local copy, index, item_stack = {}, next(stack_list)
    while index do
        if minetest.registered_items[item_stack:get_name()] then table.insert(copy, item_stack) end
        index, item_stack = next(stack_list, index)
    end
    return copy
end

setmetatable(registered_packages, {
    __index = function(parent_table, package_name)
        if conf_packages[package_name] then
            local registered_stacks = filter_registered_stacks(conf_packages[package_name])
            parent_table[package_name] = registered_stacks
            return registered_stacks
        else
            return nil
        end
    end
})

local function insert_split_stack(stack_list, item_stack)
    local count = item_stack:get_count()
    local max = item_stack:get_stack_max()
    while count > 0 do
        local copy = ItemStack(item_stack)
        if count > max then
            copy:set_count(max)
            count = count - max
        else
            copy:set_count(count)
            count = 0
        end
        table.insert(stack_list, copy)
    end
end

local function authorized_package_names(name)
    local package_names = {}
    if minetest.check_player_privs(name, "playerfactions_give") or minetest.check_player_privs(name, factions.priv) then
        for package_name, _ in pairs(conf_packages) do
            package_names[package_name] = true
        end
    else
        for priv in pairs(minetest.get_player_privs(name)) do
            local package_name = string.match(priv, "playerfactions_give_" .. "(%S+)")
            if package_name and registered_packages[package_name] then
                package_names[package_name] = true
            end
        end
    end
    return package_names
end

local function authorize_item(name, item_name)
    if minetest.registered_items[item_name] then
        if minetest.check_player_privs(name, "playerfactions_give") then return true end
        for authorized_package in pairs(authorized_package_names(name)) do
            for _, stack in ipairs(registered_packages[authorized_package]) do
                if stack:get_name() == item_name then return true end
            end
        end
    end
    return false
end

local function authorize_package(name, package_name)
    return (minetest.check_player_privs(name, factions.priv) or minetest.check_player_privs(name, "playerfactions_give")
            or minetest.check_player_privs(name, "playerfactions_give_" .. package_name))
        and registered_packages[package_name]
end

local function provision_stacks(name, query)
    local item_stack, stack_list = ItemStack(query), {}
    if authorize_item(name, item_stack:get_name()) then
        insert_split_stack(stack_list, item_stack)
        return stack_list
    end
    if authorize_package(name, query) then return registered_packages[query] end
    return nil, S("Item or package @1 not found or not allowed.", query)
end

local function iterate_inventory_action(player_names, action)
    local names_by_outcome, player, inventory, outcome = {}
    if not next(player_names) then return S("No player was affected.") end
    for player_name in pairs(player_names) do
        player = minetest.get_player_by_name(player_name)
        if player then
            inventory = player:get_inventory()
            outcome = inventory and action(inventory) or S("inventory error")
        else
            outcome = S("disconnected")
        end
        factions_ext.register_outcome(names_by_outcome, player_name, outcome)
    end
    return factions_ext.summarize_iteration(names_by_outcome)
end


local function gen_give_stack_list(stack_list)
    return function(inventory)
        if not next(stack_list) then return S("Nothing to receive") end
        local some_success, some_failure = false, false
        for _, item_stack in ipairs(stack_list) do
            local leftover = inventory:add_item("main", item_stack)
            if leftover:is_empty() then
                some_success = true
            elseif leftover:get_count() == item_stack:get_count() then
                some_failure = true
            else
                some_success, some_failure = true, true
            end
        end
        return (some_success and (some_failure and S("Some items received") or S("All items received")) or S("None of the items received"))
    end
end

local function clear_inventory(inventory)
    local was_empty = true
    for list_name, _ in pairs(inventory:get_lists()) do
        if not inventory:is_empty(list_name) then was_empty = false end
        inventory:set_list(list_name, {})
    end
    return was_empty and S("Inventory already empty") or S("Inventory emptied")
end

local function gen_equip(stack_list)
    return function(inventory)
        return clear_inventory(inventory) .. ", " .. gen_give_stack_list(stack_list)(inventory)
    end
end

factions_ext.register_privilege("playerfactions_give", {description = S("unrestricted use of factions give sub commands") , give_to_singleplayer = false})

local conf_data = (factions_ext.open_file(factions_ext.give_config_filepath, false) or { packages = {} })["packages"]
if type(conf_data) == "table" then
    for package_name, package in pairs(conf_data) do
        if type(package) == "table" then
            if not conf_packages[package_name] then
                conf_packages[package_name] = {}
                for index, itemstring in ipairs(package) do
                    if type(itemstring) == "string" then
                        insert_split_stack(conf_packages[package_name], ItemStack(itemstring))
                    else
                        conf_packages[package_name] = nil
                        minetest.log("warning", "factions -!- invalid item: "
                            .. package_name .. " [" .. tostring(index) .. "]. Package dismissed")
                        break
                    end
                end
                if conf_packages[package_name] then
                    factions_ext.register_privilege(
                        "playerfactions_give_" .. package_name,
                        {
                            description = S("Holder has access to contents of @1", package_name),
                            give_to_singleplayer = false
                        })
                    factions_ext.register_privilege(
                        "playerfactions_grant_give_" .. package_name,
                        {
                            description = S("Holder may grant access to contents of @1", package_name),
                            give_to_singleplayer = false
                        })
                end
            else
                minetest.log("warning", "factions -!- package already registered: " .. package_name .. ".")
            end
        else
            minetest.log("warning", "factions -!- invalid package: " .. package_name .. ".")
        end
    end
else
    minetest.log("warning", "factions -!- could not load registered packages from " .. factions_ext.give_config_filepath .. ".")
end


local give_description = S("This is an extension of the basic /give command.\
<amount>: 1-65535, defaults to 1 is optional, only affects stackable nodes, has no effect otherwise (e.g. tools)\
<wear>: 0-65535, defaults to 0 (intact), requires amount to be specified, only affects tools.\
Configuration file (<world directory>@1) example: \
{ \"packages\": {\"my_package\": [ \"default:brick 100\", \"default:pick_steel 1 30000\" ] } }", factions_ext.give_config_filepath)

factions_ext.register_subcommand("give", {
	params = "<player|faction [and_me]> <identifier [<amount>[ <wear>]]|package>",
	description = give_description,
	func = function(name, action, parsed_params, params)
		local player_names, extra = factions_ext.process_names(name, params, "give")
		if not player_names then return false, extra end
		local stack_list, summary = provision_stacks(name, extra)
		if not stack_list then return false, summary end
		local give_stack_list = gen_give_stack_list(stack_list)
		return true, iterate_inventory_action(player_names, give_stack_list)
	end
})

factions_ext.register_subcommand("giveme", {
	params = "<identifier [<amount>[ <wear>]]|package>",
	description = S("This is an extension of the basic /give command. See /help factions (give) for contents syntax"),
	func = function(name, action, parsed_params, params)
		local stack_list, summary = provision_stacks(name, params)
		if not stack_list then return false, summary end
		local give_stack_list = gen_give_stack_list(stack_list)
		return true, iterate_inventory_action({[name] = true}, give_stack_list)
	end
})

factions_ext.register_subcommand("empty_inventory", {
	params = "<player|faction [and_me]>",
	description = S("Empty inventory of designated player(s)"),
	func = function(name, action, parsed_params, params)
		local player_names, extra = factions_ext.process_names(name, params, "empty_inventory")
		if not player_names then return false, extra end
		return true, iterate_inventory_action(player_names, clear_inventory)
	end
})

factions_ext.register_subcommand("equip", {
	params = "<player|faction [and_me]> <identifier [<amount>[ <wear>]]|package>",
	description = S("same as give, but replaces player inventory instead of adding to it (see /help factions give)"),
	func = function(name, action, parsed_params, params)
		local player_names, extra = factions_ext.process_names(name, params, "empty_inventory")
		if not player_names then return false, extra end
		local stack_list, summary = provision_stacks(name, extra)
		if not stack_list then return false, summary end
		local equip = gen_equip(stack_list)
		return true, iterate_inventory_action(player_names, equip)
	end
})

factions_ext.register_subcommand("packages", {
	params = "[<package>]",
	description = S("List available packages (if no argument is given) or the contents of designated package"),
	func = function(name, action, parsed_params, param)
		local package_name = string.match(param, "%s*(%S+)")
		local summary
		if package_name then
			if authorize_package(name, package_name) then -- list package contents
				summary = S("Package @1 contains:", package_name)
				for _, item_stack in ipairs(registered_packages[package_name]) do
					summary = summary .. " [" .. item_stack:get_name() .. " " .. item_stack:get_count() .. "]"
				end
			else
				return false, S("Package @1 is not available.", package_name)
			end
		else --list packages
			summary = S("Available packages:")
			for authorized_package_name in pairs(authorized_package_names(name)) do
				summary = summary .. " " .. authorized_package_name
			end
		end
		return true, summary
	end
})
