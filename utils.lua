local S = minetest.get_translator("playerfactions_ext")

local chat_line_length_threshold = tonumber(minetest.settings:get("playerfactions.chat_line_max_length") or "100")

factions_ext.summarize_iteration = function (names_by_outcome)
	local summary = S("Effect on players:").."\n"
	for outcome, names_list in pairs(names_by_outcome) do
		local line = outcome.." ("..tostring(#names_list).."):"
		local omitted_players = 0
		for _, name in ipairs(names_list) do
			if string.len(line) < chat_line_length_threshold then
				line = line.." "..name
			else
				omitted_players = omitted_players + 1
			end
		end
		if omitted_players > 0 then
			line = line.." "..S("and @1 more.",omitted_players)
		end
		summary = summary..line.."\n"
	end
	return summary
end


-- Will block unless recipients belong to owned faction or caller is playerfactions_admin
factions_ext.provision_player_names = function(name, query)
	if minetest.player_exists(query) then
		if name == query or minetest.check_player_privs(name, factions.priv) then
			return { [query] = true }
		end
		for _, faction in pairs(factions.get_administered_factions(name) or {}) do
			if factions.player_is_in_faction(faction, query) then return { [query] = true } end
		end
		return nil, S("@1 not a member of any faction you own.", query)
	end
	local facts = factions.get_facts()
	if not facts then return nil, S("Could not find player or faction named @1, no factions", query) end
	local query_faction = facts[query]
	if not query_faction then return nil, S("Could not find player or faction named @1",query) end
	if minetest.check_player_privs(name, factions.priv) or query_faction.owner == name then
		return query_faction.members
	end
	return nil, S("You do not own faction @1.", query)
end

factions_ext.register_outcome = function(names_by_outcome, name, outcome)
	if names_by_outcome[outcome] then
		table.insert(names_by_outcome[outcome], name)
	else
		names_by_outcome[outcome] = { name }
	end
end


-- Will block unless recipients belong to owned faction or caller is playerfactions_admin
factions_ext.provision_player_names = function(name, query)
	if minetest.player_exists(query) then
		if name == query or minetest.check_player_privs(name, factions.priv) then
			return { [query] = true }
		end
		for _, faction in pairs(factions.get_administered_factions(name) or {}) do
			if factions.player_is_in_faction(faction, query) then return { [query] = true } end
		end
		return nil, S("@1 is not a member of any faction you own.", query)
	end
	local facts = factions.get_facts()
	if not facts then return nil, S("Could not find player or faction named @1, no factions", query) end
	local query_faction = facts[query]
	if not query_faction then return nil, S("Could not find player or faction named @1", query) end
	if minetest.check_player_privs(name, factions.priv) or query_faction.owner == name then
		return query_faction.members
	end
	return nil, S("You do not own faction @1.",query)
end


-- Returns true, extra parameters on success or false, reason on failure
factions_ext.process_names = function(name, params, command_name)
	local targets, next_params = string.match(params, "([%S]+)[%s]*(.*)")
	if not targets then return false,  S("Invalid parameters (see /help factions @1)", command_name) end
	local player_names, reason = factions_ext.provision_player_names(name, targets)
	if not player_names then return false, reason end
	local and_me, extra_params = string.match(next_params, "(and_me)[%s]*(.*)")
	if and_me then
		next_params = extra_params
	elseif targets ~= name then
		player_names[name] = nil
	end
	return player_names, next_params
end

factions_ext.open_file = function(path, warn)
	warn = (warn==nil) or warn
	local file = io.open(path,"r")
	if not file then
		if warn then
			minetest.log("warning","factions -!- failed opening " .. path .. ".")
		end
		return false
	end
	local content = file:read("*all")
	file:close()
	local data = minetest.parse_json(content)
	if not data then
		minetest.log("error","factions -!- Failed to convert json to table " .. path .. "\nCheck that this file is a json file.")
		return false
	end
	return data
end

--  follow keys in nested subtables of json data
factions_ext.table_nested_value = function(data, ...)
	for _, key in ipairs{...} do data = (data or {})[key] end
	return data
end

factions_ext.write_file = function(path, data)
	local file = io.open(path,"w+")
	if not file then
		return false
	end
	file:write(minetest.write_json(data,true))
	file:close()
	return true
end

factions_ext.remove_prefix = function(str, prefix)
	return string.gsub(str, "^%s*"..prefix.."%s*", "", 1)
end
