local S = minetest.get_translator("playerfactions_ext")

factions_ext = {}

factions_ext.modpath = minetest.get_modpath("playerfactions_ext")

local ver = { major = 0, minor = 1 }
factions_ext.version = ver
factions_ext.version_string = string.format("%d.%d", ver.major, ver.minor)

local registered_playerfaction_command = minetest.registered_chatcommands["factions"]

local factions_handle_command = registered_playerfaction_command.func

local factions_wrapper_func = function(name, action, parsed_params, param)
    return factions_handle_command(name, action.." "..param)
end

factions_ext.config_filename = (minetest.settings:get("playerfactions.config_filename") or "factions/playerfactions.conf")
factions_ext.config_filepath = minetest.get_worldpath() .. "/" .. factions_ext.config_filename

factions_ext.give_config_filename = (minetest.settings:get("playerfactions.give_config_filename") or factions_ext.config_filename)
factions_ext.give_config_filepath = minetest.get_worldpath() .. "/" .. factions_ext.give_config_filename

factions_ext.privs_config_filename = (minetest.settings:get("playerfactions.privs_config_filename") or factions_ext.config_filename)
factions_ext.privs_config_filepath = minetest.get_worldpath() .. "/" .. factions_ext.privs_config_filename


--
-- subcommand management
--
factions_ext.subcommands_registry = {}

local get_cmd_noop = function(cmd)
    local subcommand_noop =  function(name, action, parsed_params, param)
        return false, S("Subcommand @1 not implemented", cmd)
    end
    return subcommand_noop
end

factions_ext.register_subcommand = function(cmd, def)
    def = def or {}
	def.params = def.params or ""
    def.description = def.description or ""
	def.privs = def.privs or {}
    def.func = def.func or get_cmd_noop(cmd)
    factions_ext.subcommands_registry[cmd] = def
end

--
-- privilege management
--
factions_ext.privileges_registry = {}

factions_ext.register_privilege = function(priv,def)
    def = def or {}
    def.description = def.description or ""
    def.give_to_singleplayer =  def.give_to_singleplayer or false
    factions_ext.privileges_registry[priv] = def
end


factions_ext.register_subcommand("create",{
    params = "create <faction> <password>: "..S("Create a new faction"),
    privs = {},
    func = factions_wrapper_func
})
factions_ext.register_subcommand("list",{
    params = "list: "..S("List available factions"),
    privs = {},
    func = factions_wrapper_func
})
factions_ext.register_subcommand("info",{
    params = "info <faction>: "..S("See information on a faction"),
    privs = {},
    func = factions_wrapper_func
})
factions_ext.register_subcommand("player_info",{
    params = "player_info <player>: "..S("See information on a player"),
    privs = {},
    func = factions_wrapper_func
})
factions_ext.register_subcommand("join",{
    params = "join <faction> <password>: "..S("Join an existing faction"),
    privs = {},
    func = factions_wrapper_func
})
factions_ext.register_subcommand("leave",{
    params = "leave [faction]: "..S("Leave your faction"),
    privs = {},
    func = factions_wrapper_func
})
factions_ext.register_subcommand("kick",{
    params = "kick <player> [faction]: "..S("Kick someone from your faction or from the given faction"),
    privs = {},
    func = factions_wrapper_func
})
factions_ext.register_subcommand("disband",{
    params = "disband <password> [faction]: "..S("Disband your faction or the given faction"),
    privs = {},
    func = factions_wrapper_func
})
factions_ext.register_subcommand("passwd",{
    params = "passwd <password> [faction]: "..S("Change your faction's password or the password of the given faction"),
    privs = {},
    func = factions_wrapper_func
})
factions_ext.register_subcommand("chown",
{   params = "chown <player> [faction]: "..S("Transfer ownership of your faction"),
    privs= {},
    func = factions_wrapper_func
})
factions_ext.register_subcommand("invite",{
    params = "invite <player> <faction>: "..S("Add player to a faction, you need playerfactions_admin privs"),
    privs = {},
    func = factions_wrapper_func
})

local function handle_command(name, param)
	local params = {}
	for p in string.gmatch(param, "[^%s]+") do
		table.insert(params, p)
	end
	if next(params) == nil then
		return false, S("Unknown subcommand. Run '/help factions' for help.")
	end
	local action = params[1]

    local command_handler_def = factions_ext.subcommands_registry[action]
    if command_handler_def == nil then
        return false, S("Unknown subcommand. Run '/help factions' for help.")
    end

    -- handle privs
    if next(command_handler_def.privs) and not minetest.check_player_privs(name, command_handler_def.privs) then
        local priv_list = ""
        for p,_ in pairs(command_handler_def.privs) do
            priv_list = priv_list..","..p
        end
        return false, S("You need @1 for subcommand @2", priv_list, action)
    end

    return command_handler_def.func(name, action, params, factions_ext.remove_prefix(param, action))

end

local buildParams = function()
    local params_str = ""
    for _,def in pairs(factions_ext.subcommands_registry) do
        if def.params and def.params ~= "" then
            params_str = params_str.."\n"..def.params
        end
    end
    return params_str
end

local buildDescription = function()
    local desc_str = ""
    for cmd,def in pairs(factions_ext.subcommands_registry) do
        if def.description and def.description ~= "" then
            desc_str = desc_str.."\n"..cmd..": "..def.description
        end
    end
    return desc_str
end


for _, name in ipairs({
    "utils",
	"teleport",
    "give",
    "privs"
}) do
	dofile(minetest.get_modpath("playerfactions_ext") .. "/" .. name .. ".lua")
end


--
-- register privileges
--
minetest.register_on_mods_loaded(function()
    for priv, def in pairs(factions_ext.privileges_registry) do
        if not minetest.registered_privileges[priv] then
		    minetest.register_privilege(priv, {
			    description = def.description,
			    give_to_singleplayer = def.give_to_singleplayer
		    })
	    end
    end
end)


minetest.override_chatcommand("factions",{
    params = buildParams(),
    description = (registered_playerfaction_command.description or "").. "\n" .. buildDescription(),
    privs = registered_playerfaction_command.privs,
    func = handle_command
})


if minetest.get_modpath("test_harness") and minetest.settings:get_bool("test_harness_run_tests", false) then
	dofile(factions_ext.modpath .. "/test/init.lua")
end
