factions_ext.register_test("Basic tests")

-- we replace the handler fo the command list and check that it is effectively called
factions_ext.register_test("Handler override", function()
    local call_logs = {}
    local list_command_mock = function(name, action, parsed_params, param)
        table.insert(call_logs, { name = name, action = action, parsed_params = parsed_params, param = param })
        return true, "List command called"
    end

    local old_registry = factions_ext.subcommands_registry
    factions_ext.subcommands_registry = {}
    for k, def in pairs(old_registry) do
        if k == "list" then
            def.func = list_command_mock
            factions_ext.subcommands_registry[k] = def
        else
            factions_ext.subcommands_registry[k] = def
        end
    end

    -- the function handling the chat command on a chat message is the first one in minetest.registered_on_chat_messages
    local message_handler = minetest.registered_on_chat_messages[1]
    message_handler("User", "/factions list item1 item2")

    assert(#call_logs == 1)

    local log = call_logs[1]

    assert(log.name == "User")
    assert(log.action == "list")

    assert(log.param == "item1 item2")


    message_handler("User", "/factions bar")

    assert(#call_logs == 1)

    factions_ext.subcommands_registry = old_registry

end)

factions_ext.register_test("Remove prefix", function()
    assert(factions_ext.remove_prefix("   foo  bar", "foo") == "bar")
    assert(factions_ext.remove_prefix("   foo  bar", "world") == "   foo  bar")
    assert(factions_ext.remove_prefix("   foo foo bar", "foo") == "foo bar")
end)