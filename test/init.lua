---@diagnostic disable: lowercase-global, duplicate-set-field

factions_ext.register_test = test_harness.get_test_registrator("playerfactions_ext", factions_ext.version_string)

for _, name in ipairs({
	"base",
	"teleport",
	"give",
	"privs"
}) do
	dofile(minetest.get_modpath("playerfactions_ext") .. "/test/" .. name .. ".lua")
end
