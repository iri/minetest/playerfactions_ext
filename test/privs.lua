factions_ext.register_test("Priv_groups def tests")

factions_ext.register_test("Check privs for privs registrations", function()
    for _, priv in ipairs({
      "playerfactions_grant",
      "playerfactions_grant_teacher",
      "playerfactions_grant_Tago"}) do

        assert(minetest.registered_privileges[priv] ~= nil)
    end
end)

factions_ext.register_test("Priv_groups conf", function()

    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { [factions.priv]= true })


    local package_command = factions_ext.subcommands_registry['priv_groups']
    local res, msg = package_command.func("test", "priv_groups", {}, "")
    assert(res)
    assert(string.match(msg, "teacher"))
    assert(string.match(msg, "Tago"))
end)

factions_ext.register_test("Priv_groups conf priv", function()

    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_grant"]= true })


    local package_command = factions_ext.subcommands_registry['priv_groups']
    local res, msg = package_command.func("test", "priv_groups", {}, "")
    assert(res)
    assert(string.match(msg, "teacher"))
    assert(string.match(msg, "Tago"))
end)

factions_ext.register_test("Privs group conf priv specific", function()

    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_grant_teacher"]= true })


    local package_command = factions_ext.subcommands_registry['priv_groups']
    local res, msg = package_command.func("test", "priv_groups", {}, "")
    assert(res)
    assert(string.match(msg, "teacher"))
    assert(not string.match(msg, "Tago"))
end)

factions_ext.register_test("Privs group conf priv specific other", function()

    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_grant_Tago"]=true })


    local package_command = factions_ext.subcommands_registry['priv_groups']
    local res, msg = package_command.func("test", "priv_groups", {}, "")
    assert(res)
    assert(not string.match(msg, "teacher"))
    assert(string.match(msg, "Tago"))
end)

factions_ext.register_test("Grant tests")

local compound_privs = function(privs, plist)
    local res = true
    for _,p in ipairs(plist) do
        res = res and privs[p]
    end
    return res
end

local get_keys = function(tab)
    local res = {}
    for k,v in pairs(tab) do
        table.insert(res, k)
    end
    return res
end

factions_ext.register_test("Grant privs group to single player", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")
    
    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist))

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "player1 teacher")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)

    
    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist) and not compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})



factions_ext.register_test("Grant privs group to players", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")
    
    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist))

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "testfaction teacher")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)

    
    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})


factions_ext.register_test("Grant single privs to players", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")
    
    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist) and not privs1.shout)
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist) and not privs2.shout)

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "testfaction shout")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)

    
    assert(#plist1 == (#initial_plist + 1) and privs1.shout)
    assert(#plist2 == (#initial_plist + 1) and privs2.shout)

end, { players= { "player1", "player2" }})


factions_ext.register_test("Grant single privs to single players", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")
    
    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist) and not privs1.shout)
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist) and not privs2.shout)

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "player1 shout")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)

    
    assert(#plist1 == (#initial_plist + 1) and privs1.shout)
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist) and not privs2.shout)

end, { players= { "player1", "player2" }})


factions_ext.register_test("Grant privs to players give priv", function()
    local privs = minetest.get_player_privs("player1")
    privs["playerfactions_grant"]= true
    minetest.set_player_privs("player1", privs)

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")
    
    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == (#initial_plist+1) and compound_privs(privs1, initial_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist))

    local res, msg = factions_ext.subcommands_registry['grant'].func("player1", "grant", nil, "testfaction teacher")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)

    
    assert(#plist1 == (#initial_plist + 1) and not compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})


factions_ext.register_test("Grant privs to players give priv specific and me", function()
    local privs = minetest.get_player_privs("player1")
    privs["playerfactions_grant_teacher"]= true
    minetest.set_player_privs("player1", privs)

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")
    
    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == (#initial_plist+1) and compound_privs(privs1, initial_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist))

    local res, msg = factions_ext.subcommands_registry['grant'].func("player1", "grant", nil, "testfaction and_me teacher")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)

    
    assert(#plist1 == (#initial_plist + #secondary_plist + 1) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})


factions_ext.register_test("Grant privs to players no param", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")
    
    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist))

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "")
    assert(not res)
    minetest.debug("GRANT res : ", res, msg)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)
    
    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist) and not compound_privs(privs1, secondary_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist) and not compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})


factions_ext.register_test("Grant privs to players no faction", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")
    
    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist))

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "teacher")
    assert(not res)
    minetest.debug("GRANT res : ", res, msg)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)

    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist) and not compound_privs(privs1, secondary_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist) and not compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})


factions_ext.register_test("Grant privs to players no privs_group", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")
    
    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist))

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "testfaction")
    assert(not res)
    minetest.debug("GRANT res : ", res, msg)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)

    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist) and not compound_privs(privs1, secondary_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist) and not compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})


factions_ext.register_test("Grant privs to players bad privs_group", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")
    
    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist))

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "testfaction pupils")
    assert(not res)
    minetest.debug("GRANT res : ", res, msg)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)

    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist) and not compound_privs(privs1, secondary_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist) and not compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})

factions_ext.register_test("Grant privs to players bad faction", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")
    
    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist))

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "foofaction teacher")
    assert(not res)
    minetest.debug("GRANT res : ", res, msg)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)

    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist) and not compound_privs(privs1, secondary_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist) and not compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})


factions_ext.register_test("Grant privs to players not owner", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_give"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")
    
    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist))

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "testfaction teacher")
    assert(not res)
    minetest.debug("GRANT res : ", res, msg)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)

    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist) and not compound_privs(privs1, secondary_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist) and not compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})

factions_ext.register_test("Grantme tests")
-- grantme
factions_ext.register_test("Grantme single priv", function()
    local privs = minetest.get_player_privs("player1")
    privs["playerfactions_grant"]= true
    minetest.set_player_privs("player1", privs)

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    
    local privs = minetest.get_player_privs("player1")

    local plist = get_keys(privs)

    assert(#plist == #initial_plist+1 and not privs.shout)

    local res, msg = factions_ext.subcommands_registry['grantme'].func("player1", "grantme", nil, "shout")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    privs = minetest.get_player_privs("player1")

    plist = get_keys(privs)

    assert(#plist == #initial_plist+2 and privs.shout)

end, { players= { "player1" }})


factions_ext.register_test("Grantme privs group", function()
    local privs = minetest.get_player_privs("player1")
    privs["playerfactions_grant"]= true
    minetest.set_player_privs("player1", privs)

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}
    
    local privs = minetest.get_player_privs("player1")

    local plist = get_keys(privs)

    assert(#plist == #initial_plist+1 and compound_privs(privs, initial_plist) and not compound_privs(privs, secondary_plist))

    local res, msg = factions_ext.subcommands_registry['grantme'].func("player1", "grantme", nil, "teacher")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    privs = minetest.get_player_privs("player1")

    plist = get_keys(privs)

    assert(#plist == #initial_plist+#secondary_plist+1 and compound_privs(privs, initial_plist) and compound_privs(privs, secondary_plist))

end, { players= { "player1" }})

-- revoke
factions_ext.register_test("Revoke tests")

factions_ext.register_test("Revoke privs group to players", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "testfaction teacher")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

    res, msg = factions_ext.subcommands_registry['revoke'].func("test", "revoke", nil, "testfaction teacher")
    if not res then minetest.debug("REVOKE res : ", res, msg) end
    assert(res)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)
    
    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist) and not compound_privs(privs1, secondary_plist))
    assert(#plist2 == #initial_plist and compound_privs(privs2, initial_plist) and not compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})

factions_ext.register_test("Revoke privs group to players no arg", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "testfaction teacher")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

    res, msg = factions_ext.subcommands_registry['revoke'].func("test", "revoke", nil, "")
    if res then minetest.debug("REVOKE res : ", res, msg) end
    assert(not res)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)
    
    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})

factions_ext.register_test("Revoke privs group to players missing arg", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "testfaction teacher")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

    res, msg = factions_ext.subcommands_registry['revoke'].func("test", "revoke", nil, "testfaction")
    if res then minetest.debug("REVOKE res : ", res, msg) end
    assert(not res)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)
    
    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})

factions_ext.register_test("Revoke privs group to players no faction", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "testfaction teacher")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

    res, msg = factions_ext.subcommands_registry['revoke'].func("test", "revoke", nil, "teacher")
    if res then minetest.debug("REVOKE res : ", res, msg) end
    assert(not res)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)
    
    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})

factions_ext.register_test("Revoke privs group to players bad faction", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "testfaction teacher")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

    res, msg = factions_ext.subcommands_registry['revoke'].func("test", "revoke", nil, "foofaction teacher")
    if res then minetest.debug("REVOKE res : ", res, msg) end
    assert(not res)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)
    
    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})

factions_ext.register_test("Revoke privs group to players bad priv group", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "testfaction teacher")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

    res, msg = factions_ext.subcommands_registry['revoke'].func("test", "revoke", nil, "testfaction pupils")
    if res then minetest.debug("REVOKE res : ", res, msg) end
    assert(not res)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)
    
    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})


factions_ext.register_test("Revoke privs group to single player", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "testfaction teacher")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

    res, msg = factions_ext.subcommands_registry['revoke'].func("test", "revoke", nil, "player1 teacher")
    if not res then minetest.debug("REVOKE res : ", res, msg) end
    assert(res)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)
    
    assert(#plist1 == #initial_plist and compound_privs(privs1, initial_plist) and not compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})

factions_ext.register_test("Revoke single priv to single player", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "testfaction teacher")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}
    local secondary_bis_plist = {"give", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

    res, msg = factions_ext.subcommands_registry['revoke'].func("test", "revoke", nil, "player1 shout")
    if not res then minetest.debug("REVOKE res : ", res, msg) end
    assert(res)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)
    
    assert(#plist1 == (#initial_plist + #secondary_plist - 1) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_bis_plist) and not privs1.shout)
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

end, { players= { "player1", "player2" }})

factions_ext.register_test("Revoke single priv to players", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local res, msg = factions_ext.subcommands_registry['grant'].func("test", "grant", nil, "testfaction teacher")
    if not res then minetest.debug("GRANT res : ", res, msg) end
    assert(res)

    local privs1 = minetest.get_player_privs("player1")
    local privs2 = minetest.get_player_privs("player2")

    local initial_plist = {"fly","fast","noclip","basic_debug","debug","interact"}
    local secondary_plist = {"give", "shout", "teleport", "worldedit"}
    local secondary_bis_plist = {"give", "teleport", "worldedit"}

    local plist1 = get_keys(privs1)
    local plist2 = get_keys(privs2)

    assert(#plist1 == (#initial_plist + #secondary_plist) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_plist))
    assert(#plist2 == (#initial_plist + #secondary_plist) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_plist))

    res, msg = factions_ext.subcommands_registry['revoke'].func("test", "revoke", nil, "testfaction shout")
    if not res then minetest.debug("REVOKE res : ", res, msg) end
    assert(res)

    privs1 = minetest.get_player_privs("player1")
    privs2 = minetest.get_player_privs("player2")

    plist1 = get_keys(privs1)
    plist2 = get_keys(privs2)
    
    assert(#plist1 == (#initial_plist + #secondary_plist - 1) and compound_privs(privs1, initial_plist) and compound_privs(privs1, secondary_bis_plist) and not privs1.shout)
    assert(#plist2 == (#initial_plist + #secondary_plist - 1) and compound_privs(privs2, initial_plist) and compound_privs(privs2, secondary_bis_plist) and not privs2.shout)

end, { players= { "player1", "player2" }})
