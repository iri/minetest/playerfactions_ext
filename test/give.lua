
factions_ext.register_test("Package def tests")

factions_ext.register_test("Check privs registrations", function()
    for _, priv in ipairs({
      "playerfactions_give",
      "playerfactions_give_ichi",
      "playerfactions_give_nidan",
      "playerfactions_grant_give_ichi",
      "playerfactions_grant_give_nidan"}) do
        assert(minetest.registered_privileges[priv] ~= nil, "Priv "..priv.." not found")
    end
end)

factions_ext.register_test("Packages conf", function()

    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { [factions.priv]= true })


    local package_command = factions_ext.subcommands_registry['packages']
    local res, msg = package_command.func("test", "packages", {}, "")
    assert(res)
    assert(string.match(msg, "ichi"))
    assert(string.match(msg, "nidan"))
end)

factions_ext.register_test("Packages conf priv", function()

    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_give"]= true })


    local package_command = factions_ext.subcommands_registry['packages']
    local res, msg = package_command.func("test", "packages", {}, "")
    assert(res)
    assert(string.match(msg, "ichi"))
    assert(string.match(msg, "nidan"))
end)

factions_ext.register_test("Packages conf priv specific", function()

    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_give_ichi"]= true })


    local package_command = factions_ext.subcommands_registry['packages']
    local res, msg = package_command.func("test", "packages", {}, "")
    assert(res)
    assert(string.match(msg, "ichi"))
    assert(not string.match(msg, "nidan"))
end)

factions_ext.register_test("Packages conf priv specific other", function()

    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_give_nidan"]= true })


    local package_command = factions_ext.subcommands_registry['packages']
    local res, msg = package_command.func("test", "packages", {}, "")
    assert(res)
    assert(not string.match(msg, "ichi"))
    assert(string.match(msg, "nidan"))
end)


factions_ext.register_test("Give tests")

factions_ext.register_test("Give package to players", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local player1 = minetest.get_player_by_name("player1")
    local player2 = minetest.get_player_by_name("player2")

    assert(player1:get_inventory():is_empty("main"))
    assert(player2:get_inventory():is_empty("main"))

    local res, msg = factions_ext.subcommands_registry['give'].func("test", "give", nil, "testfaction ichi")
    if not res then minetest.debug("GIVE res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))
    assert(not player2:get_inventory():is_empty("main"))

    for _,p in ipairs({player1, player2}) do
        for _,item in ipairs({"default:dirt", "default:pick_steel"}) do
            assert(p:get_inventory():contains_item("main", item))
        end
    end

end, { players= { "player1", "player2" }})

factions_ext.register_test("Give package to players faction owner", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    minetest.set_player_privs("player1", { ["playerfactions_give_ichi"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local player1 = minetest.get_player_by_name("player1")
    local player2 = minetest.get_player_by_name("player2")

    assert(player1:get_inventory():is_empty("main"))
    assert(player2:get_inventory():is_empty("main"))

    local res, msg = factions_ext.subcommands_registry['give'].func("player1", "give", nil, "testfaction ichi")
    if not res then minetest.debug("GIVE res : ", res, msg) end
    assert(res)

    assert(player1:get_inventory():is_empty("main"))
    assert(not player2:get_inventory():is_empty("main"))

    for _,p in ipairs({player2}) do
        for _,item in ipairs({"default:dirt", "default:pick_steel"}) do
            assert(p:get_inventory():contains_item("main", item))
        end
    end

end, { players= { "player1", "player2" }})

factions_ext.register_test("Give package to players faction owner and_me", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    minetest.set_player_privs("player1", { ["playerfactions_give_ichi"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local player1 = minetest.get_player_by_name("player1")
    local player2 = minetest.get_player_by_name("player2")

    assert(player1:get_inventory():is_empty("main"))
    assert(player2:get_inventory():is_empty("main"))

    local res, msg = factions_ext.subcommands_registry['give'].func("player1", "give", nil, "testfaction and_me ichi")
    if not res then minetest.debug("GIVE res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))
    assert(not player2:get_inventory():is_empty("main"))

    for _,p in ipairs({player1, player2}) do
        for _,item in ipairs({"default:dirt", "default:pick_steel"}) do
            assert(p:get_inventory():contains_item("main", item))
        end
    end

end, { players= { "player1", "player2" }})


factions_ext.register_test("Give item to players", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local player1 = minetest.get_player_by_name("player1")
    local player2 = minetest.get_player_by_name("player2")

    assert(player1:get_inventory():is_empty("main"))
    assert(player2:get_inventory():is_empty("main"))

    local res, msg = factions_ext.subcommands_registry['give'].func("test", "give", nil, "testfaction default:stone 30")
    if not res then minetest.debug("GIVE res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))
    assert(not player2:get_inventory():is_empty("main"))

    for _,p in ipairs({player1, player2}) do
        assert(p:get_inventory():contains_item("main", ItemStack("default:stone 30"), true))
    end

end, { players= { "player1", "player2" }})


factions_ext.register_test("empty inventory faction", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local player1 = minetest.get_player_by_name("player1")
    local player2 = minetest.get_player_by_name("player2")

    assert(player1:get_inventory():is_empty("main"))
    assert(player2:get_inventory():is_empty("main"))

    local res, msg = factions_ext.subcommands_registry['give'].func("test", "give", nil, "testfaction ichi")
    if not res then minetest.debug("GIVE res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))
    assert(not player2:get_inventory():is_empty("main"))

    res, msg = factions_ext.subcommands_registry['empty_inventory'].func("test", "empty_inventory", nil, "testfaction")
    if not res then minetest.debug("EMPTY_INVENTORY res : ", res, msg) end
    assert(res)

    assert(player1:get_inventory():is_empty("main"))
    assert(player2:get_inventory():is_empty("main"))

end, { players= { "player1", "player2" }})


factions_ext.register_test("empty inventory player", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    local player1 = minetest.get_player_by_name("player1")

    assert(player1:get_inventory():is_empty("main"))

    local res, msg = factions_ext.subcommands_registry['give'].func("test", "give", nil, "testfaction ichi")
    if not res then minetest.debug("GIVE res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))

    res, msg = factions_ext.subcommands_registry['empty_inventory'].func("test", "empty_inventory", nil, "player1")
    if not res then minetest.debug("EMPTY_INVENTORY res : ", res, msg) end
    assert(res)

    assert(player1:get_inventory():is_empty("main"))

end, { players= { "player1", "player2" }})

factions_ext.register_test("empty inventory faction member", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local player1 = minetest.get_player_by_name("player1")
    local player2 = minetest.get_player_by_name("player2")

    assert(player1:get_inventory():is_empty("main"))
    assert(player2:get_inventory():is_empty("main"))

    local res, msg = factions_ext.subcommands_registry['give'].func("test", "give", nil, "testfaction ichi")
    if not res then minetest.debug("GIVE res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))
    assert(not player2:get_inventory():is_empty("main"))

    res, msg = factions_ext.subcommands_registry['empty_inventory'].func("test", "empty_inventory", nil, "player1")
    if not res then minetest.debug("EMPTY_INVENTORY res : ", res, msg) end
    assert(res)

    assert(player1:get_inventory():is_empty("main"))
    assert(not player2:get_inventory():is_empty("main"))

end, { players= { "player1", "player2" }})


factions_ext.register_test("empty inventory faction owner", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    minetest.set_player_privs("player1", { ["playerfactions_give_ichi"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local player1 = minetest.get_player_by_name("player1")
    local player2 = minetest.get_player_by_name("player2")

    assert(player1:get_inventory():is_empty("main"))
    assert(player2:get_inventory():is_empty("main"))

    local res, msg = factions_ext.subcommands_registry['give'].func("player1", "give", nil, "testfaction and_me ichi")
    if not res then minetest.debug("GIVE res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))
    assert(not player2:get_inventory():is_empty("main"))

    res, msg = factions_ext.subcommands_registry['empty_inventory'].func("player1", "empty_inventory", nil, "player2")
    if not res then minetest.debug("EMPTY_INVENTORY res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))
    assert(player2:get_inventory():is_empty("main"))

end, { players= { "player1", "player2" }})

factions_ext.register_test("empty inventory faction owner himself", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    minetest.set_player_privs("player1", { ["playerfactions_give_ichi"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local player1 = minetest.get_player_by_name("player1")
    local player2 = minetest.get_player_by_name("player2")

    assert(player1:get_inventory():is_empty("main"))
    assert(player2:get_inventory():is_empty("main"))

    local res, msg = factions_ext.subcommands_registry['give'].func("player1", "give", nil, "testfaction and_me ichi")
    if not res then minetest.debug("GIVE res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))
    assert(not player2:get_inventory():is_empty("main"))

    res, msg = factions_ext.subcommands_registry['empty_inventory'].func("player1", "empty_inventory", nil, "player1")
    if not res then minetest.debug("EMPTY_INVENTORY res : ", res, msg) end
    assert(res)

    assert(player1:get_inventory():is_empty("main"))
    assert(not player2:get_inventory():is_empty("main"))

end, { players= { "player1", "player2" }})

factions_ext.register_test("empty inventory faction owner faction", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    minetest.set_player_privs("player1", { ["playerfactions_give_ichi"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local player1 = minetest.get_player_by_name("player1")
    local player2 = minetest.get_player_by_name("player2")

    assert(player1:get_inventory():is_empty("main"))
    assert(player2:get_inventory():is_empty("main"))

    local res, msg = factions_ext.subcommands_registry['give'].func("player1", "give", nil, "testfaction and_me ichi")
    if not res then minetest.debug("GIVE res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))
    assert(not player2:get_inventory():is_empty("main"))

    res, msg = factions_ext.subcommands_registry['empty_inventory'].func("player1", "empty_inventory", nil, "testfaction")
    if not res then minetest.debug("EMPTY_INVENTORY res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main")) -- !!!!!!!!!!! expected ???
    assert(player2:get_inventory():is_empty("main"))

end, { players= { "player1", "player2" }})


factions_ext.register_test("Equip players", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local player1 = minetest.get_player_by_name("player1")
    local player2 = minetest.get_player_by_name("player2")

    assert(player1:get_inventory():is_empty("main"))
    assert(player2:get_inventory():is_empty("main"))

    local res, msg = factions_ext.subcommands_registry['give'].func("test", "give", nil, "testfaction default:stone 30")
    if not res then minetest.debug("GIVE res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))
    assert(not player2:get_inventory():is_empty("main"))

    for _,p in ipairs({player1, player2}) do
        assert(p:get_inventory():contains_item("main", ItemStack("default:stone 30"), true))
    end

    res, msg = factions_ext.subcommands_registry['equip'].func("test", "equip", nil, "testfaction ichi")
    if not res then minetest.debug("EQUIP res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))
    assert(not player2:get_inventory():is_empty("main"))

    for _,p in ipairs({player1, player2}) do
        assert(not p:get_inventory():contains_item("main", ItemStack("default:stone")))
        for _,item in ipairs({"default:dirt", "default:pick_steel"}) do
            assert(p:get_inventory():contains_item("main", item))
        end
    end

end, { players= { "player1", "player2" }})


factions_ext.register_test("Equip players item", function()
    minetest.set_player_password("test", minetest.get_password_hash("test", "pwrd"))
    minetest.set_player_privs("test", { ["playerfactions_admin"]= true })

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")

    local player1 = minetest.get_player_by_name("player1")
    local player2 = minetest.get_player_by_name("player2")

    assert(player1:get_inventory():is_empty("main"))
    assert(player2:get_inventory():is_empty("main"))

    local res, msg = factions_ext.subcommands_registry['give'].func("test", "give", nil, "testfaction default:stone 30")
    if not res then minetest.debug("GIVE res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))
    assert(not player2:get_inventory():is_empty("main"))

    for _,p in ipairs({player1, player2}) do
        assert(not p:get_inventory():contains_item("main", ItemStack("default:dirt")))
        assert(p:get_inventory():contains_item("main", ItemStack("default:stone 30"), true))
    end

    res, msg = factions_ext.subcommands_registry['equip'].func("test", "equip", nil, "testfaction default:dirt")
    if not res then minetest.debug("EQUIP res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))
    assert(not player2:get_inventory():is_empty("main"))

    for _,p in ipairs({player1, player2}) do
        assert(p:get_inventory():contains_item("main", ItemStack("default:dirt")))
        assert(not p:get_inventory():contains_item("main", ItemStack("default:stone")))
    end


end, { players= { "player1", "player2" }})

factions_ext.register_test("Giveme package", function()
    minetest.set_player_privs("player1", { ["playerfactions_admin"]= true })

    local player1 = minetest.get_player_by_name("player1")

    assert(player1:get_inventory():is_empty("main"))

    local res, msg = factions_ext.subcommands_registry['giveme'].func("player1", "giveme", nil, "ichi")
    if not res then minetest.debug("GIVEME res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))

    for _,item in ipairs({"default:dirt", "default:pick_steel"}) do
        assert(player1:get_inventory():contains_item("main", item))
    end

end, { players= { "player1", "player2" }})

factions_ext.register_test("Giveme item", function()
    minetest.set_player_privs("player1", { ["playerfactions_admin"]= true })

    local player1 = minetest.get_player_by_name("player1")

    assert(player1:get_inventory():is_empty("main"))

    local res, msg = factions_ext.subcommands_registry['giveme'].func("player1", "giveme", nil, "default:stone 30")
    if not res then minetest.debug("GIVEME res : ", res, msg) end
    assert(res)

    assert(not player1:get_inventory():is_empty("main"))

    assert(player1:get_inventory():contains_item("main", ItemStack("default:stone 30"), true))

end, { players= { "player1", "player2" }})
