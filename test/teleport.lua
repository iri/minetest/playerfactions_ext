factions_ext.register_test("teleport tests")
factions_ext.register_test("teleport ok", function()

    -- create faction
    local owned_factions = factions.get_owned_factions("player1")
    assert(not owned_factions or #owned_factions == 0)
    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")
    
    owned_factions = factions.get_owned_factions("player1")
    assert(#owned_factions == 1)
    local player1 = minetest.get_player_by_name("player1")
    local player2 = minetest.get_player_by_name("player2")

    local privs = minetest.get_player_privs("player1")
    privs.playerfactions_teleport  = true
    minetest.set_player_privs("player1", privs)

    local res, msg = factions_ext.subcommands_registry['teleport'].func("player1", "teleport", nil, "testfaction 100,100,100")

    assert(res)
    assert(vector.distance(vector.new(100,100,100), player1:get_pos()) < 2)
    assert(vector.distance(vector.new(100,100,100), player2:get_pos()) < 2)
    assert(vector.distance(player1:get_pos(), player2:get_pos()) < 2)

    -- disband
    factions_ext.subcommands_registry['disband'].func("player1", "disband", nil, "foobar testfaction")

end, { players= {"player1", "player2"}})

factions_ext.register_test("teleport ko not owner", function()

    factions_ext.subcommands_registry['create'].func("player1", "create", nil, "testfaction foobar")
    factions_ext.subcommands_registry['join'].func("player2", "join", nil, "testfaction foobar")
    
    local player1 = minetest.get_player_by_name("player1")
    local player2 = minetest.get_player_by_name("player2")

    local player1_pos = player1:get_pos()
    local player2_pos = player2:get_pos()

    local privs = minetest.get_player_privs("player1")
    privs.playerfactions_teleport  = true
    minetest.set_player_privs("player1", privs)
    privs = minetest.get_player_privs("player2")
    privs.playerfactions_teleport  = true
    minetest.set_player_privs("player2", privs)


    local res, msg = factions_ext.subcommands_registry['teleport'].func("player2", "teleport", nil, "testfaction 100,100,100")

    assert(not res)
    assert(vector.distance(player1_pos, player1:get_pos()) < 2)
    assert(vector.distance(player2_pos, player2:get_pos()) < 2)

    factions_ext.subcommands_registry['disband'].func("player1", "disband", nil, "foobar testfaction")

end, { players= {"player1", "player2"}})